﻿using UnityEngine;
using UnityEngine.UI;

public class LeaderboardScript : MonoBehaviour {

    public Text player1Score, player2Score, player3Score, player4Score, player1Place, player2Place, player3Place, player4Place;

	// Use this for initialization
	void Start () {

        player1Score.text = "" + Data.answerPlayer1;
        player2Score.text = "" + Data.answerPlayer2;
        player3Score.text = "" + Data.answerPlayer3;
        player4Score.text = "" + Data.answerPlayer4;
        
        showScore();

    }
	
	// Update is called once per frame
	void Update () {


	}

    public void GoToMenu()
    {
        Application.LoadLevel(0);
    }

    void showScore()
    {
        if(Data.answerPlayer1 > Data.answerPlayer2)
        {
            if(Data.answerPlayer1 > Data.answerPlayer3)
            {
                if(Data.answerPlayer1 > Data.answerPlayer4)
                {
                    player1Place.text = "1st";
                }
                else if(Data.answerPlayer1 < Data.answerPlayer4)
                {
                    player1Place.text = "2nd";
                }
            }
            else if(Data.answerPlayer1 < Data.answerPlayer3)
            {
                if(Data.answerPlayer1 > Data.answerPlayer4)
                {
                    player1Place.text = "2nd";
                }
                else if(Data.answerPlayer1 < Data.answerPlayer4)
                {
                    player1Place.text = "3rd";
                }
            }

        }
        else if(Data.answerPlayer1 < Data.answerPlayer2)
        {
            if (Data.answerPlayer1 > Data.answerPlayer3)
            {
                if (Data.answerPlayer1 > Data.answerPlayer4)
                {
                    player1Place.text = "2nd";
                }
                else if (Data.answerPlayer1 < Data.answerPlayer4)
                {
                    player1Place.text = "3rd";
                }
            }
            else if (Data.answerPlayer1 < Data.answerPlayer3)
            {
                if (Data.answerPlayer1 > Data.answerPlayer4)
                {
                    player1Place.text = "3rd";
                }
                else if (Data.answerPlayer1 < Data.answerPlayer4)
                {
                    player1Place.text = "4th";
                }
            }
        }

        if (Data.answerPlayer2 > Data.answerPlayer1)
        {
            if (Data.answerPlayer2 > Data.answerPlayer3)
            {
                if (Data.answerPlayer2 > Data.answerPlayer4)
                {
                    player2Place.text = "1st";
                }
                else if (Data.answerPlayer2 < Data.answerPlayer4)
                {
                    player2Place.text = "2nd";
                }
            }
            else if (Data.answerPlayer2 < Data.answerPlayer3)
            {
                if (Data.answerPlayer2 > Data.answerPlayer4)
                {
                    player2Place.text = "2nd";
                }
                else if (Data.answerPlayer2 < Data.answerPlayer4)
                {
                    player2Place.text = "3rd";
                }
            }

        }
        else if (Data.answerPlayer2 < Data.answerPlayer1)
        {
            if (Data.answerPlayer2 > Data.answerPlayer3)
            {
                if (Data.answerPlayer2 > Data.answerPlayer4)
                {
                    player2Place.text = "2nd";
                }
                else if (Data.answerPlayer2 < Data.answerPlayer4)
                {
                    player2Place.text = "3rd";
                }
            }
            else if (Data.answerPlayer2 < Data.answerPlayer3)
            {
                if (Data.answerPlayer2 > Data.answerPlayer4)
                {
                    player2Place.text = "3rd";
                }
                else if (Data.answerPlayer2 < Data.answerPlayer4)
                {
                    player2Place.text = "4th";
                }
            }
        }

        if (Data.answerPlayer3 > Data.answerPlayer2)
        {
            if (Data.answerPlayer3 > Data.answerPlayer1)
            {
                if (Data.answerPlayer3 > Data.answerPlayer4)
                {
                    player3Place.text = "1st";
                }
                else if (Data.answerPlayer3 < Data.answerPlayer4)
                {
                    player3Place.text = "2nd";
                }
            }
            else if (Data.answerPlayer3 < Data.answerPlayer1)
            {
                if (Data.answerPlayer3 > Data.answerPlayer4)
                {
                    player3Place.text = "2nd";
                }
                else if (Data.answerPlayer3 < Data.answerPlayer4)
                {
                    player3Place.text = "3rd";
                }
            }

        }
        else if (Data.answerPlayer3 < Data.answerPlayer2)
        {
            if (Data.answerPlayer3 > Data.answerPlayer1)
            {
                if (Data.answerPlayer3 > Data.answerPlayer4)
                {
                    player3Place.text = "2nd";
                }
                else if (Data.answerPlayer3 < Data.answerPlayer4)
                {
                    player3Place.text = "3rd";
                }
            }
            else if (Data.answerPlayer3 < Data.answerPlayer1)
            {
                if (Data.answerPlayer3 > Data.answerPlayer4)
                {
                    player3Place.text = "3rd";
                }
                else if (Data.answerPlayer3 < Data.answerPlayer4)
                {
                    player1Place.text = "4th";
                }
            }
        }

        if (Data.answerPlayer4 > Data.answerPlayer2)
        {
            if (Data.answerPlayer4 > Data.answerPlayer3)
            {
                if (Data.answerPlayer4 > Data.answerPlayer1)
                {
                    player4Place.text = "1st";
                }
                else if (Data.answerPlayer4 < Data.answerPlayer1)
                {
                    player4Place.text = "2nd";
                }
            }
            else if (Data.answerPlayer4 < Data.answerPlayer3)
            {
                if (Data.answerPlayer4 > Data.answerPlayer1)
                {
                    player4Place.text = "2nd";
                }
                else if (Data.answerPlayer4 < Data.answerPlayer1)
                {
                    player4Place.text = "3rd";
                }
            }

        }
        else if (Data.answerPlayer4 < Data.answerPlayer2)
        {
            if (Data.answerPlayer4 > Data.answerPlayer3)
            {
                if (Data.answerPlayer4 > Data.answerPlayer1)
                {
                    player4Place.text = "2nd";
                }
                else if (Data.answerPlayer4 < Data.answerPlayer1)
                {
                    player4Place.text = "3rd";
                }
            }
            else if (Data.answerPlayer4 < Data.answerPlayer3)
            {
                if (Data.answerPlayer4 > Data.answerPlayer1)
                {
                    player4Place.text = "3rd";
                }
                else if (Data.answerPlayer4 < Data.answerPlayer1)
                {
                    player4Place.text = "4th";
                }
            }
        }


    }

}
